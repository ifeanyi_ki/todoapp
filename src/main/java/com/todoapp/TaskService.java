package com.todoapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

/**
 * Created by KINGLEE on 1/7/17.
 */

@Service
@Transactional
public class TaskService {

    @Autowired
    public TaskRepository taskRepository;


    public TaskService() {
    }

    private TaskService(TaskRepository taskRepository){
        this.taskRepository = taskRepository;
    }


    public List<Task> getAllTasks(){
       List<Task> taskList = new ArrayList<>();
        for(Task task : taskRepository.findAll()){
            taskList.add(task);
        }

        return  taskList;
    }

    public void save(Task task){
        taskRepository.save(task);
    }

}
