package com.todoapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.*;

/**
 * Created by KINGLEE on 1/7/17.
 */

@Controller(value = "/task")
public class HomeController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/home")
    public String greeting(){
        return "index";
    }

    @GetMapping("/getTasks")
    public String getAllTasks(Model model){
        List<Task> taskList = taskService.getAllTasks();
        model.addAttribute("taskList", taskList);
        return "list-task";

    }

    @GetMapping("/show-form")
    public String showTaskForm(Task task){
//        model.addAttribute("task", new Task());
        return "task-form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute Task task){

        System.out.println("task description ==> " + task.getTask_desc());
        task.setDate_created(new Date());
        taskService.save(task);

        return "list-task";

    }

}
