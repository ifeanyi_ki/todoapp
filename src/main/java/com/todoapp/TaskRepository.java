package com.todoapp;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by KINGLEE on 1/7/17.
 */



public interface TaskRepository extends CrudRepository<Task, Integer>{

}
