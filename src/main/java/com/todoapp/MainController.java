package com.todoapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;
/**
 * Created by KINGLEE on 1/7/17.
 */

@RestController
public class MainController {

    private TaskService taskService;

    @GetMapping("/")
    public String greeting(){
        return "Hello Longbridger Devs!";
    }

    @GetMapping("/findTasks")
    public List<Task> getTasks(){
        return getTaskService().getAllTasks();
    }

    @Autowired
    public void setTaskService(TaskService taskService){
         this.taskService = taskService;
    }


    public TaskService getTaskService(){
        return  taskService;
    }





}
